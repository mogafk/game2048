/**
 * Создает класс игры.
 * @param size {Number} размер. Размер только квадртаный, одно число
 * @constructor
 */
function Game(size){
    this.size = size;
    this.grid = [];

    this.failedDirection = {
        left: false,
        right: false,
        top: false,
        down: false
    };
    this.direction = ["left", "right", "top", "down"];

    this.score = 0;
}
/**
 * Получает из localStorage по ключу score счет
 * @returns {number} максимальный счет
 */
Game.prototype.getBestScore = function(){
    var score = localStorage.getItem("score");
    if(!score){
        return 0;
    }else{
        return ~~score;
    }
    console.warn("not tested");
};
/**
 * Устанавливает максимальный счет в localStorage по ключу score
 * @param score {Number} число, что будет записано по ключу score
 */
Game.prototype.setBestScore = function(score){
    localStorage.setItem("score", score);
};
/**
 * Функция, для отображения максимального счета
 * Может быть переопределенна в отдельном инстансе.
 * @param score
 * @override
 */
Game.prototype.viewBestScore = function(score){
    console.log("viewBestScore", score);
};
/**
 * Заполняет таблицу взависимости от размера ячейками
 */
Game.prototype.init = function(){
    this.grid = [];
    for(var row=0; row<this.size; row++){
        this.grid.push([]);
        for(var col=0; col<this.size; col++){
            this.grid[row].push(new Cell(this, row, col));
        }
    }

    this.viewBestScore(this.getBestScore());
};
/**
 * Заполняет поле value каждой ячейки значениями из matrix
 * @param matrix матрица значений, должна соотвествовать
 *               размеру переданному в конструктор Game.
 *               Иначе возбуждает ошибку размеров.
 */
Game.prototype.fixtures = function(matrix){
    if(matrix.length != this.size){
        throw new SizeError("you try to pass matrix with size not equal grid");
    }
    for(var i=0; i<this.size; i++){
        for(var j=0; j<this.size; j++){
            this.grid[i][j].value = matrix[i][j];
        }
    }
};
/**
 * Возвращает случайную ячейку
 * @returns {Cell}
 */
Game.prototype.randomCell = function(){
    var randomRow = randomInteger(1, this.size)-1,
        randomCol = randomInteger(1, this.size)-1;

    return this.getCell({row: randomRow, column: randomCol});
};
/**
 * Проверяет ошибку конца игры. Сейчас логика не очень хорошая:
 * Проверяет наличие проваленных попыток во всех направлениях
 * в Game(size).failedDirection.
 * Не хорошо, что значения в failedDirection устанавливаются
 * при попытке сделать свайп игроком(пока игрок в конце игры
 * не попытается сделать неуспешное движение во все стороны
 * игра не закончилась)
 * Если игра закончилась, то излучает событие onGameOver
 */
Game.prototype.checkGameover = function(){
    var gameover = false;
    for(var i=0; i<this.direction.length; i++){
        gameover += this.failedDirection[this.direction[i]];
    }
    if(gameover == 4){
        this.emitEvent("onGameOver");
    }
};
/**
 * Событие конца игры, может быть определенно.
 * Так же устанавливает текущий счет как лучший(ошибка,
 * потому что без проверки)
 * @override !!c вызовом оригинала
 */
Game.prototype.onGameOver = function(){
    this.setBestScore(this.score);
    alert("gameOver");
};
/**
 * Служит для сброса накопленных ошибок по направлениям
 */
Game.prototype.resetFailed = function(){
    for(var i=0; i<this.direction.length;i++){
        this.failedDirection[this.direction[i]] = false;
    }
};
/**
 * Добавляет значение 2 случайную ячейки
 * Используется крайне неправиьный подход в качестве
 * определения, что свобдных ячеек нет. Требует исправления
 * @param direction {String} [left|right|top|down] направление
 */
Game.prototype.addRandomValue = function(direction){
    var count = randomInteger(1,2)-1,
        randomCell, countAttempts = 0;
    for(var i=0; i<=count; i++){
        while((randomCell = this.randomCell()).value != 0) {
            //Вот это неправильный подход. Тут надо обязательно переделать
            if (countAttempts++ > 1000) {
                this.failedDirection[direction] = true;
                this.checkGameover();
                return;
            }
        }
        this.resetFailed();
        randomCell.value = 2;
    }
};
/**
 * Ищет ближайшее значение 0 взависимости от направления.
 * @param {String} direction [left|right|top|down] направление
 * @param {Number} i столбец или строка, в котором ищется ближайшее
 *                   значение 0. Ближайшее взависимости от направления.
 */
Game.prototype.findGap = function(direction, i){
    var invert = {
        left: "right",
        right: "left",
        top: "down",
        down: "top"
    };

    var cellGap = this.getFirstCell(i);
    for(var j=0; j<this.size-1; j++){
        if(cellGap.value == 0){
            return cellGap;
        }else {
            cellGap = cellGap[invert[direction]]();
        }
    }
};
/**
 * Возвращает ячейку
 * @param {Object} coords
 * @param {Number} coords.row    строка требуемой ячейки
 * @param {Number} coords.column колонка требуемой ячейки
 * @returns {Cell}
 */
Game.prototype.getCell = function(coords){
    return this.grid[coords.row][coords.column];
};
/**
 * Событие onChange. Возбуждается по окончанию свайпа
 * @override
 */
Game.prototype.onChange = function(){
    this.dumpHumanityGrid();
};
/**
 * Служит для возбуждения различных событий
 * @param {String} event само имя события
 */
Game.prototype.emitEvent = function(event){
    this[event]();
};
/**
 * Убирает пробелы между числами в строке\колонке(определяется
 * с помощью getFirstCell в Game.move)
 * !!Внутри обьект invert, который обьявляется и в другой функции
 * надо перенести его в свойства класса
 * @param {String} direction [left|right|top|down] направление
 */
Game.prototype.pushToDirection = function(direction){
    var invert = {
        left: "right",
        right: "left",
        top: "down",
        down: "top"
    };

    var values = [];
    for(var i=0; i<this.size; i++){
        var cell = this.getFirstCell(i);
        for(var j=0; j<this.size; j++){
            try {
                if (cell.value != 0) {
                    values.push(cell.value);
                    cell.value = 0;
                }
                cell = cell[invert[direction]]();
            }catch(e){}
        }

        cell = this.getFirstCell(i);
        for(var len = values.length, j=0; j<len; j++){
            try {
                cell.value = values.shift();
                cell = cell[invert[direction]]();
            }catch(e){}
        }
        values = [];
    }
};
/**
 * Свайп взависимости от направления. Внутри определяется метод
 * getFirstCell, что возвращает первую от границы ячейку взависимости от
 * направления. Для направления влево это самая левая ячейка строки вза-
 * висимости от аргумента getFirstCell.
 * @param {String} direction [left|right|top|down] направление
 */
Game.prototype.move = function(direction){
    var edges = {
        left: 0,
        right: this.size-1,
        top: 0,
        down: this.size-1
    };
    var invert = {
        left: "right",
        right: "left",
        top: "down",
        down: "top"
    };

    console.log("direction", direction);
    if(direction == "left" || direction == "right"){
        var columnConst = edges[direction];
        this.getFirstCell = function(rowArg){
            return this.getCell({row: rowArg, column: columnConst});
        }
    }else{//directrion = top | down
        var rowConst = edges[direction];
        this.getFirstCell = function(colArg){
            return this.getCell({row: rowConst, column: colArg})
        }
    }

    var cell;
    for(var i=0; i<this.size; i++) {
        cell = this.getFirstCell(i);
        for(var j=0; j<this.size-1; j++){
            var cellNext = cell[invert[direction]]();
            if(cell.value == 0){
                cell = cell[invert[direction]]();
                continue;
            }
            try{
                while(cellNext.value == 0){
                    cellNext = cellNext[invert[direction]]();
                }
                if(cell.value == cellNext.value){
                    cell.value = cell.value*2;
                    this.score += cell.value;
                    cellNext.value = 0;
                    console.log("_________________________________");
                }else{
                    ;//pass
                }
            }catch(e){
            }

            cell = cell[invert[direction]]();
        }
    }

    this.pushToDirection(direction);
    this.addRandomValue(direction);
    this.emitEvent("onChange");
    this.dumpHumanityGrid();
};
/**
 * Вешает обработчик события кнопок-стрелок, а так же
 * если пользователь на мобильной платформе свайпы.
 */
Game.prototype.event = function(){
    var that = this;
    $(document).on("keydown", function(e){
        if(e.keyCode == 38){//up-arrow
            console.log("up-arrow");
            that.move("top");
        }
        if(e.keyCode ==40){//down-arrow
            that.move("down");
            console.log("down-arrow");
        }
        if(e.keyCode == 37){//left-arrow
            that.move("left");
            console.log("left-arrow")
        }
        if(e.keyCode == 39){//right-arrow
            that.move("right");
            console.log("right-arrow");
        }
    });

    if(CONST.mobile){
        var catchSwipe = document.getElementById("catch-swipe");
        var mv = new Hammer(catchSwipe);
        mv.get('swipe').set({ direction: Hammer.DIRECTION_ALL });

        mv.on("swipeup swipedown swipeleft swiperight", function(e){
            if(e.type == "swipeup"){
                that.move("top");
            }
            if(e.type == "swipedown"){
                that.move("down")
            }
            if(e.type == "swipeleft"){
                that.move("left");
            }
            if(e.type == "swiperight"){
                that.move("right");
            }
        });
    }
};
/**
 * Выводит в консоль все значения. Используется
 * только при константе window.CONST.debug.
 * Можно играть через консоль :D
 */
Game.prototype.dumpHumanityGrid = function(){
    if(window.CONST.debug == 1){
        var prettyStr = "";
        for(var i=0; i<this.size; i++){
            for(var j=0; j<this.size; j++){
                prettyStr += " | "+this.grid[i][j].value;
            }
            prettyStr += "\n";
        }

        console.log(prettyStr);
    }
};