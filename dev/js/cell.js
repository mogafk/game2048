/**
 * Класс ячейки
 * @param game связь с инстансом Game, что содержит игровое поле
 *             и методы самой игры
 * @param row  какая строка у ячейки
 * @param column какая колонка у ячейки
 * @constructor
 */
function Cell(game, row, column){
    this.value = 0;
    this.game = game;
    this.column = column;
    this.row = row;
}
/**
 * Возвращает соседнюю ячейку слева или возбуждает ошибку
 * boundError
 * @returns {Cell} инстанс класса Cell
 */
Cell.prototype.left = function(){
    if((this.column-1) >= 0){
        return this.game.grid[this.row][this.column-1];
    }
    throw new BoundError("you try to get the cell index column "+(this.column-1));
};
/**
 * Возвращает соседнюю ячейку справа или возбуждает ошибку
 * boundError
 * @returns {Cell} инстанс класса Cell
 */
Cell.prototype.right = function(){
    if(this.column+1 < this.game.size){
        return this.game.grid[this.row][this.column+1];
    }
    throw new BoundError("you try get cell with column "+(this.column+1)+", but max"+(this.game.size-1));
};
/**
 * Возвращает соседнюю ячейку сверху или возбуждает ошибку
 * boundError
 * @returns {Cell} инстанс класса Cell
 */
Cell.prototype.top = function(){
    if(this.row-1 >= 0){
        return this.game.grid[this.row-1][this.column];
    }
    throw new BoundError("you try get cell with row "+(this.row-1));
};
/**
 * Возвращает соседнюю ячейку снизу или возбуждает ошибку
 * boundError
 * @returns {Cell} инстантс класса Cell
 */
Cell.prototype.down = function(){
    if(this.row+1 < this.game.size){
        return this.game.grid[this.row+1][this.column];
    }
    throw new BoundError("you try get cell with row "+(this.row+1)+", but max"+(this.game.size-1));
};