/**
 * Класс ошибки, когда пытаемся получить ячейку за границей игрового поля
 * @param message сообщение, которе будет отображено в console
 * @constructor
 */
function BoundError(message){
    this.message = message;
    this.name = "__BoundError";
}
BoundError.prototype = new Error;
/**
 * Класс ошибки, используется при заполнение фикстурами поля.
 * Возбуждается, если матрица значений больше размера поля
 * @param message сообщение, которе будет отображено в console
 * @constructor
 */
function SizeError(message){
    this.message = message;
    this.name = "__SizeError";
}
SizeError.prototype = new Error;