/**
 * Детектирует основные мобильные агенты
 * @returns {boolean} true - клиент на смартфоне\таблетке
 */
function detectMobile(){
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        return true;
    }
    return false;
}
/**
 * Подключает динамически js файл. В коде не используется.
 * @param path {String} путь до файла
 * @deprecated вобще не используется.
 */
function loadJsDynamic(path){
    var script = document.createElement('script');
    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", path);

    document.head.appendChild(script);
}
/**
 * Случайное число (min; max]
 * @param min минимальная граница
 * @param max максимаьная граница
 * @returns {Number}
 */
function randomInteger(min, max) {
    var rand = min + Math.random() * (max - min)
    rand = Math.round(rand);
    return rand;
}
