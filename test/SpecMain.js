describe("Cell", function(){
    var game;
    game = new Game(6);
    game.init();

    beforeEach(function(){
        game = new Game(6);
        game.init();
    });


    describe("try to get cell index over bound of table(grid). Should be throw Error", function(){
        it("left Error", function(){
            expect(game.grid[0][0].left).toThrowError(BoundError);
        });
        it("right Error", function(){
            expect(game.grid[game.size-1][game.size-1].right).toThrowError(BoundError);
        });
        it("top Error", function(){
            expect(game.grid[0][0].top).toThrowError(BoundError);
        });
        it("down Error", function(){
            expect(game.grid[game.size-1][0].down).toThrowError(BoundError);
        });
    });


    describe("try to get adjacent cell", function(){
        var cell;
        beforeEach(function(){
            cell = game.grid[1][1];
        });

        it("get left cell", function(){
            expect(cell.left().column).toEqual(0);
        });
        it("get right cell", function(){
            expect(cell.right().column).toEqual(2);
        });
        it("get top cell", function(){
            expect(cell.top().row).toEqual(0);
        });
        it("get down cell", function(){
            expect(cell.down().row).toEqual(2);
        });
    });
});


describe("Game", function(){
    var edges = {
        left: 0,
        right: this.size-1,
        top: 0,
        down: this.size-1
    };

    var equalGameGrid = function(first, second){
        if(first instanceof Game && second instanceof Game){
            for(var row=0; row<game.size; row++){
                for(var col=0; col<game.size; col++){
                    var firstVal = first.getCell({row: row, column: col}).value;
                    var secondVal = second.getCell({row: row, column: col}).value;
                    if(firstVal == 2 || firstVal == 4)
                        continue;
                    if(firstVal != secondVal)
                        return false;
                }
            }
            return true;
        }
    };
    describe("set and read score from localStorage", function(){
        var game = new Game(CONST.size);
        game.setBestScore(777);

        it("try to set score", function(){
            expect(game.getBestScore()).toEqual(777);
        });
    });

    describe("pushToDirections", function(){
        beforeEach(function() {
            jasmine.addCustomEqualityTester(equalGameGrid);
        });

        var game = new Game(CONST.size);
        game.init();
        game.fixtures([
            [0,0,0,16,32,0],
            [126,126,8,16,16,8],
            [256,256,32,16,1024,8],
            [16,0,8,126,32,16],
            [0,0,0,0,8,16],
            [0,0,0,0,0,0]
        ]);
        var direction = "left";
        if(direction == "left" || direction == "right"){
            var columnConst = edges[direction];
            Game.prototype.getFirstCell = function(rowArg){
                return this.getCell({row: rowArg, column: columnConst});
            }
        }else{//directrion = top | down
            var rowConst = edges[direction];
            Game.prototype.getFirstCell = function(colArg){
                return this.getCell({row: rowConst, column: colArg})
            }
        }
        game.pushToDirection(direction);

        var gameShouldBe = new Game(CONST.size);
        gameShouldBe.init();
        gameShouldBe.fixtures([
            [16,   32,   0,   0,    0,  0],
            [126,   126,   8,   16,    16,  8],
            [256,   256,   32,   16,    1024,  8],
            [16,   8,   126,   32,    16,  0],
            [8,   16,   0,   0,    0,  0],
            [0,   0,   0,   0,    0,  0]
        ]);
        it("pushToDirection(left)", function(){
            expect(game).toEqual(gameShouldBe);
        });

        var game = new Game(CONST.size);
        game.init();
        game.fixtures([
            [0,0,0,16,32,0],
            [126,126,8,16,16,8],
            [256,256,32,16,1024,8],
            [16,0,8,126,32,16],
            [0,0,0,0,8,16],
            [0,0,0,0,0,0]
        ]);
        var direction = "top";
        if(direction == "left" || direction == "right"){
            var columnConst = edges[direction];
            Game.prototype.getFirstCell = function(rowArg){
                return this.getCell({row: rowArg, column: columnConst});
            }
        }else{//directrion = top | down
            var rowConst = edges[direction];
            Game.prototype.getFirstCell = function(colArg){
                return this.getCell({row: rowConst, column: colArg})
            }
        }
        game.pushToDirection(direction);

        var gameShouldBe = new Game(CONST.size);
        gameShouldBe.init();
        gameShouldBe.fixtures([
            [126,   126,  8,   16,    32,  8],
            [256,   256,  32,   16,    16, 8],
            [16,    0,    8,   16,    1024,  16],
            [0,     0,    0,   126,    32,  16],
            [0,     0,    0,   0,    8,  0],
            [0,     0,    0,   0,    0,  0]
        ]);
        it("pushToDirection(top)", function(){
            expect(game).toEqual(gameShouldBe);
        });

    });

    describe("swipes", function(){
        beforeEach(function() {
            jasmine.addCustomEqualityTester(equalGameGrid);
        });

        var game = new Game(CONST.size);
        game.init();
        game.fixtures([
            [0,0,0,16,32,0],
            [126,126,8,16,16,8],
            [256,256,32,16,1024,8],
            [16,0,8,126,32,16],
            [0,0,0,0,8,16],
            [0,0,0,0,0,0]
        ]);
        game.event();
        game.move("left");

        var gameShouldBe = new Game(CONST.size);
        gameShouldBe.init();
        gameShouldBe.fixtures([
            [16,   32,   0,   0,    0,  0],
            [252,  8,    32,  8,    0,  0],
            [512,  32,   16,  1024, 8,  0],
            [16,   8,    126, 32,   16, 0],
            [8,    16,   0,   0,    0,  0],
            [0,    0,    0,   0,    0,  0]
        ]);
        it("left swipe", function(){
            expect(game).toEqual(gameShouldBe);
        });

        var game = new Game(CONST.size);
        game.init();
        game.fixtures([
            [0,0,0,16,32,0],
            [126,126,8,16,16,8],
            [256,256,32,16,1024,8],
            [16,0,8,126,32,16],
            [0,0,0,0,8,16],
            [0,0,0,0,0,0]
        ]);
        game.event();
        game.move("right");

        var gameShouldBe = new Game(CONST.size);
        gameShouldBe.init();
        gameShouldBe.fixtures([
            [0,   0,   0,   0,    16,  32],
            [0,  0,    252,  8,    32,  8],
            [0,  512,   32,  16, 1024,  8],
            [0,   16,    8, 126,   32, 16],
            [0,    0,   0,   0,    8,  16],
            [0,    0,    0,   0,    0,  0]
        ]);
        it("right swipe", function(){
            expect(game).toEqual(gameShouldBe);
        });

        var game = new Game(CONST.size);
        game.init();
        game.fixtures([
            [0,   0,   0,  16,  32,   0],
            [126, 126, 8,  16,  16,   8],
            [256, 256, 32, 16,  1024, 8],
            [16,  0,   8,  126, 32,   16],
            [0,   0,   0,  0,   8,    16],
            [0,   0,   0,  0,   0,    0]
        ]);
        game.event();
        game.move("top");

        var gameShouldBe = new Game(CONST.size);
        gameShouldBe.init();
        gameShouldBe.fixtures([
            [126,  126,   8,   32,   32,    16],
            [256,  256,   32,  16,   16,    32],
            [16,   0,     8,   126,  1024,  0],
            [0,    0,     0,   0,    32,    0],
            [0,    0,     0,   0,    8,     0],
            [0,    0,     0,   0,    0,     0]
        ]);
        it("top swipe", function(){
            expect(game).toEqual(gameShouldBe);
        });

        var game = new Game(CONST.size);
        game.init();
        game.fixtures([
            [0,   0,   0,  16,  32,   0],
            [126, 126, 8,  16,  16,   8],
            [256, 256, 32, 16,  1024, 8],
            [16,  0,   8,  126, 32,   16],
            [0,   0,   0,  0,   8,    16],
            [0,   0,   0,  0,   0,    0]
        ]);
        game.event();
        game.move("down");

        var gameShouldBe = new Game(CONST.size);
        gameShouldBe.init();
        gameShouldBe.fixtures([
            [0,  0,   0,   0,    0,  0],
            [0,  0,   0,   0,    32,  0],
            [0,  0,   0,   0,    16,  0],
            [126,  0,   8,   16,    1024,  0],
            [256,  126,   32,   32,    32,  16],
            [16,  256,   8,   126,    8,  32]
        ]);
        it("down swipe", function(){
            expect(game).toEqual(gameShouldBe);
        });
    });

});